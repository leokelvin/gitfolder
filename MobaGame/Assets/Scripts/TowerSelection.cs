﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSelection : MonoBehaviour
{
    [Header("Paineis de opção")]
    [SerializeField] GameObject Opcoes;
    [SerializeField] GameObject OpcoesNv;
    [Space(2)]
    [Header("Torre1")]    
    [SerializeField] GameObject torre1;
    [SerializeField] GameObject torre1N2;
    [Space(2)]
    [Header("Torre2")]    
    [SerializeField] GameObject torre2;
    [SerializeField] GameObject torre2N2;
    
    bool hasTower;
    bool hasT1;   

    private void OnMouseDown()
    {
        if (!hasTower)
        {
            Opcoes.SetActive(!Opcoes.activeInHierarchy);
        }
        else
        {
            OpcoesNv.SetActive(!Opcoes.activeInHierarchy);
        }
    }


    GameObject temp1;
    public void instantiateTower1()
    {
        if (!hasTower)
        {
           temp1 = Instantiate(torre1, new Vector3(gameObject.transform.position.x, 2, gameObject.transform.position.z), Quaternion.identity);
           temp1.SetActive(true);
        }        
        Opcoes.SetActive(!Opcoes.activeInHierarchy);
        hasTower = true;
        hasT1 = true;
    }

    GameObject temp2;
    public void instantiateTower2()
    {
        if (!hasTower)
        {
            temp2 = Instantiate(torre2, new Vector3(gameObject.transform.position.x, 2, gameObject.transform.position.z), Quaternion.identity);
            temp2.SetActive(true);
        }        
        Opcoes.SetActive(!Opcoes.activeInHierarchy);
        hasTower = true;
    }

    public void UpgradeTower()
    {        
        if (hasT1)
        {
            Destroy(temp1);
            temp1 =  Instantiate(torre1N2, new Vector3(gameObject.transform.position.x, 2, gameObject.transform.position.z), Quaternion.identity);
        }
        else
        {
            Destroy(temp2);
            temp2 = Instantiate(torre2N2, new Vector3(gameObject.transform.position.x, 2, gameObject.transform.position.z), Quaternion.identity);
        }
    }

}
