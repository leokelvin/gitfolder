﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class MinionsBehaviour : MonoBehaviour
{
    NavMeshAgent navmesh;

    Transform mainObjective;
    List<GameObject> enemys = new List<GameObject>();

    enum minionsEstados { andando, atacando};

    minionsEstados estados;

    void Start()
    {
        navmesh = GetComponent<NavMeshAgent>();
        mainObjective = GameObject.FindGameObjectWithTag("BlueTeamBase").GetComponent<Transform>();
        

        estados = minionsEstados.andando;
    }

    // Update is called once per frame
    void Update()
    {
        enemys = GameObject.FindGameObjectsWithTag("BlueEnemy").ToList();

       
       if(enemys[0] != null)
        {
            float dist = Vector3.Distance(enemys[0].transform.position, gameObject.transform.position);
            if (dist > 10)
            {
                estados = minionsEstados.atacando;
            }
            else
            {
                estados = minionsEstados.andando;
            }
            Debug.Log(dist);

        }



        changeEstados();
    }

    void changeEstados()
    {
        switch (estados)
        {
            case minionsEstados.andando:
                navmesh.destination = mainObjective.position;
                break;
            case minionsEstados.atacando:
                navmesh.destination = enemys[0].transform.position;
                break;
        }
    }

}

