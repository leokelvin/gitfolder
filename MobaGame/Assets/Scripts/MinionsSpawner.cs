﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionsSpawner : MonoBehaviour
{

    [SerializeField] GameObject minion;

    private void Start()
    {
        InvokeRepeating("SpawMinions", 5, 15);
    }

    public void SpawMinions()
    {
        Instantiate(minion, transform.position, Quaternion.identity);
    }


}
