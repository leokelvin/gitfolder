﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{

    NavMeshAgent navmesh;
    [SerializeField] Transform cam;

    void Start()
    {
        navmesh = GetComponent<NavMeshAgent>();
       
    }

    // Update is called once per frame
    void Update()
    {                
        cam.transform.position = new Vector3(transform.position.x + 9.14f, cam.transform.position.y, gameObject.transform.position.z);

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
            if(Physics.Raycast(ray, out hit, 100))
            {
                navmesh.destination = hit.point;
            }
        }

    }
}
