﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerBehaviour : MonoBehaviour
{

    [SerializeField] int Damage;
    [SerializeField] int Life;
    [SerializeField] int Range;

    [SerializeField] Slider lifeBar;

    int currentLife;

    void Start()
    {
        lifeBar = GetComponentInChildren<Slider>();
        lifeBar.maxValue = Life;
        lifeBar.value = Life;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
